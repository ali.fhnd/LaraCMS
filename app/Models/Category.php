<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Category extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'parent_id'];

    public function posts()
    {
        return $this->
        belongsToMany(Post::class, 'category_post', 'category_id', 'post_id');
//            ->as('data')
//            ->withTimestamps()
//            ->withPivot('expired_at');

    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
//        return self::where('parent_id',$this->id);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public static function IdTitles()
    {
        return self::pluck('title','id')->toArray();
    }

    public function activePosts()
    {
        return $this->posts()->where('active',1)->get();
    }

}
