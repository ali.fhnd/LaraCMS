<?php

namespace App\Http\Middleware;

use Closure;

class AddTokenHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $response =  $next($request);
        $response->withHeaders([
            'token' => 'S64879465asfew49r8we465a1cq89we4dqw984das5'
        ]);
        return $response;

    }
}
