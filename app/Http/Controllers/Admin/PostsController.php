<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostCreateRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\User;
use App\Utility\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::get();//Post::where('wallet','>',100000)->get();

        return view('admin.posts.list', compact('posts'));
    }

    public function create()
    {
        if (Gate::denies('posts.create')) {
            abort(403);
        }
        $postStatuses       = Post::postStatuses();
        $allCategories      = Category::get();
        $allTags            = Tag::get();
        $categories         = $allCategories->groupBy('parent_id');
        $categories['root'] = $categories[''];
        unset($categories['']);

        return view('admin.posts.create', compact('postStatuses', 'categories', 'allTags'));
    }

    public function store(Request $request)
    {
        $post = Post::create([
            'post_title'   => $request->input('postTitle'),
            'post_slug'    => $request->input('postTitle'),
            'post_content' => $request->input('postContent'),
            'post_author'  => User::first()->id, //Auth::id(),
            'post_status'  => $request->input('postStatus')
        ]);
        if ($post && $post instanceof Post) {
            $categories = $request->input('categories');
            if (count($categories) > 0) {
                $post->categories()->attach($categories);
            }
            $tags = $request->input('postTags');
            foreach ($tags as $key => $tag) {
                if (intval($tag) == 0) {
                    unset($tags[$key]);
                    $newTag = Tag::create(['name' => $tag]);
                    $tags[] = $newTag->id;
                }
            }
            $tags = array_map(function ($item) {
                return intval($item);
            }, $tags);
            $tags = array_unique($tags);
            $post->tags()->sync($tags);
            if ($request->hasFile('postImage')) {
                $token = Hash::token();
                //$fileName = $token.'.'.$request->file('postImage')->getClientOriginalExtension();
                $fileName = sprintf("%s.%s", $token, $request->file('postImage')->getClientOriginalExtension());

                $postImage = $request->file('postImage')->storeAs('images', $fileName);
                $post->post_image = $fileName;
                $post->save();
            }

            return back()->with('status', 'مطلب جدید با موفقیت ایجاد گردید!');
        }
    }

    public function delete(Request $request, $post_id)
    {

        $post         = Post::find($post_id);
        $deleteResult = $post->delete();
        if ($deleteResult) {
            return back()->with('status', 'مطلب با موفقیت حذف گردید');
        }
//        Post::destroy($post_id);
    }

    public function edit(Request $request, $post_id)
    {
        $post           = Post::find($post_id);
        $allTags        = Tag::get();
        $categories     = Category::get();
        $postCategories = $post->categories->pluck('id')->toArray();
        $postStatuses   = Post::postStatuses();
        $postTags       = $post->tags->pluck('id')->toArray();


        return view('admin.posts.edit',
            compact('post', 'postStatuses', 'postCategories', 'categories', 'postTags', 'allTags'));

    }

    public function update(Request $request, $post_id)
    {
        $post = Post::find($post_id);
        if ($post && $post instanceof Post) {
            $postData = [
                'post_title'   => $request->input('postTitle'),
                'post_slug'    => $request->input('postTitle'),
                'post_content' => $request->input('postContent'),
                'post_status'  => $request->input('postStatus')
            ];

            $updateResult = $post->update($postData);
            if ($updateResult) {
                $categories = $request->input('categories');
                if (count($categories) > 0) {
                    $post->categories()->sync($categories);
                }

                $tags = $request->input('postTags');
                foreach ($tags as $key => $tag) {
                    if (intval($tag) == 0) {
                        unset($tags[$key]);
                        $newTag = Tag::create(['name' => $tag]);
                        $tags[] = $newTag->id;
                    }
                }
                $tags = array_map(function ($item) {
                    return intval($item);
                }, $tags);
                $tags = array_unique($tags);
                $post->tags()->sync($tags);

                return redirect()->route('admin.posts')->with('status', 'مطلب با موفقیت به روز رسانی گردید!');
            }
        }
    }
}
